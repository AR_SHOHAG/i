#include<stdio.h>

int main()
{
    int T, i;
    scanf("%d", &T);

    for(i=1; i<=T; ++i){
        long int N, j, p;
        scanf("%ld", &N);
        long int n[N];

        for(j=0; j<N; ++j){
            scanf("%ld", &n[j]);
        }
        scanf("%ld", &p);
        for(j=0; j<N; ++j){
            if(n[j]==p)
                printf("Case %d: %ld\n", i, j+1);
        }
    }
    return 0;
}
